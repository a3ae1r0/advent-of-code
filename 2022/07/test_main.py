import main


def test_part1():
    assert main.part_one("sample.txt") == 95437


def test_part2():
    assert main.part_two("sample.txt") == 24933642
