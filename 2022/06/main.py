def read_input(input):
    """Read input file"""
    with open(input, 'r') as f:
        return f.read().strip('\n')


def find_marker(datastream, type):
    """Create marker with four chars at the time. Find if they're all
    different characters. Returns characters count.
       args: datastream (string) --> fill datastream buffer
             type (string)       --> type of the marker to be found: packer or
                                     message
    """
    size = 4 if type == 'packet' else 14

    for i in range(len(datastream)-size):
        marker = [datastream[ch] for ch in range(i, i+size)]

        if len(set(marker)) == len(marker):
            return i+size


def part_one(input):
    """Return characters count before reaching the start-of-packet marker"""
    data = read_input(input)
    return find_marker(data, 'packet')


def part_two(input):
    """Return characters count before reaching the start-of-message marker"""
    data = read_input(input)
    return find_marker(data, 'message')


if __name__ == "__main__":
    input = "./input.txt"
    print("--- Part One ---")
    print(part_one(input))

    print("--- Part Two ---")
    print(part_two(input))
