def read_input(input):
    """Return list with contents of a file"""
    with open(input, "r") as f:
        d = f.read().splitlines()
    return d


def get_sections(s):
    """Returns two lists with the cleaning section IDs for each elf in the
    pair
    """
    hifen1 = s.find('-')
    hifen2 = s.find('-', hifen1+1)
    comma = s.find(',')

    section1 = list(range(int(s[:hifen1]), int(s[hifen1+1:comma]) + 1))
    section2 = list(range(int(s[comma+1:hifen2]), int(s[hifen2+1:]) + 1))

    return section1, section2


def part_one(input):
    """Find count when both elves sections fully overlap on inside the other"""
    data = read_input(input)
    count = 0

    for line in data:
        section1, section2 = get_sections(line)

        overlap1 = all(element in section2 for element in section1)
        overlap2 = all(element in section1 for element in section2)

        if overlap1 or overlap2:
            count += 1

    return count


def part_two(input):
    """Find count when at least one section overlap between two elves"""
    data = read_input(input)
    count = 0

    for line in data:
        section1, section2 = get_sections(line)

        for i in section1:
            if i in section2:
                count += 1
                break

    return count


if __name__ == "__main__":
    input = "./input.txt"
    print("--- Part One ---")
    print(part_one(input))

    print("--- Part Two ---")
    print(part_two(input))
