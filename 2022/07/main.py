# NOTE: after being stuck too much time one part1, because of how to handle
#       directories and sub-directories, I had to look online for some hints.
#       Although my initial work was on a good path (create_dir_struct), the
#       following walkthrough help me to understand the solution better:
#       - https://youtu.be/FXQWIWHaFBE
#       - https://github.com/jeff-frederic/AdventOfCode-2022/blob/main/day7.py


def parse_input(input):
    """Read and return input"""
    with open(input, "r") as f:
        return f.read()


def create_dir_struct(commands):
    """Iterate over each line of the data. Act upon each 'command' and `ls`
    output
    """
    current_path = ''
    dirs = {}

    for line in commands.split('\n')[:-1]:
        # Commands
        if line[0] == '$':
            # `cd` command
            if line.startswith('$ cd '):
                # Change directory to '/'
                if line.split(' ')[2] == '/':
                    current_path = '/'
                    dirs.setdefault(current_path, 0)
                # Go to parent dir
                elif line.split(' ')[2] == '..':
                    # Remove everything after last '/' (aka last dir)
                    current_path = current_path[:current_path.rfind('/')]
                # Change directory
                else:
                    dir_name = line.split(' ')[2]
                    # This avoid having double slashes on root directory, but
                    # needs a extra step below to add sub-directories sizes to
                    # root
                    if current_path == '/':
                        current_path += dir_name
                    else:
                        current_path += '/' + dir_name

                    dirs.setdefault(current_path, 0)

        # `ls` output
        # Listing directories: we don't really need to do nothing
        # Listing files: append size to directory
        # Because of this we avoid 'dir' lines and only want the remaining
        elif not line.startswith('dir'):
            size = int(line.split(' ')[0])

            # Add sub-directories content size also to '/'
            if current_path != '/':
                dirs['/'] += size

            # Loop ensure that the parent(s) directories size also include the
            # size of the children directories contents (excluding '/')
            dir = current_path
            for i in range(current_path.count('/')):
                dirs[dir] += size
                dir = dir[:dir.rfind('/')]      # Regressively add size

    return dirs


def part_one(input):
    """Return total size of directories below MAX_SIZE"""
    data = parse_input(input)
    dirs = create_dir_struct(data)
    MAX_SIZE = 100000
    total = 0

    for dir in dirs:
        if dirs[dir] < MAX_SIZE:
            total += dirs[dir]

    return total


def part_two(input):
    """Find the size of the directory needed to delete in order to have the
    required space for the update
    """
    data = parse_input(input)
    dirs = create_dir_struct(data)
    TOTAL_SPACE = 70000000
    REQUIRED_SPACE = 30000000
    unused_space = TOTAL_SPACE - dirs['/']

    smallest = 0
    if unused_space < REQUIRED_SPACE:
        to_delete = REQUIRED_SPACE - unused_space
        smallest = min(size for size in dirs.values() if size >= to_delete)

    return smallest


if __name__ == "__main__":
    input = "./input.txt"
    print("--- Part One ---")
    print(part_one(input))

    print("--- Part Two ---")
    print(part_two(input))
