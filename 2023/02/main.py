from functools import reduce

MAX_VALUES = {
    'red': 12,
    'green': 13,
    'blue': 14,
}


def read_input(infile: str) -> list:
    """Read exercise input."""
    with open(infile, mode='r', encoding='utf8') as f:
        return f.readlines()


def parse_game_sets(data: list):
    """Parse input with records for all games to individual sets info."""
    for i, game in enumerate(data):
        data[i] = game[game.find(':') + 1:].strip().strip('\n').split(';')
    return data


def part_one(infile: str) -> int:
    """Iterate over each set in each game of find if number of cubes color
    exceeded the maximum number of cubes available for that particular color.
    """
    data = parse_game_sets(read_input(infile))
    sum_ids = 0

    for i, game in enumerate(data):
        is_game_possible = True
        for sets in game:
            for color in sets.split(','):
                color = color.strip().split()
                if int(color[0]) > MAX_VALUES[color[1]]:
                    is_game_possible = False
                    break
            if not is_game_possible:
                break
        if is_game_possible:
            sum_ids += i + 1
    return sum_ids


def part_two(infile: str) -> int:
    """Iterate over all sets in the multiple games to find the minimum number
    of cubes required."""
    data = parse_game_sets(read_input(infile))
    power_sets = 0

    for _, game in enumerate(data):
        min_values = {
            'red': 0,
            'green': 0,
            'blue': 0,
        }
        for sets in game:
            for color in sets.split(','):
                color = color.strip().split()
                color[0] = int(color[0])
                if color[0] > min_values[color[1]]:
                    min_values[color[1]] = color[0]
        power_sets += reduce(lambda x, y: x * y, min_values.values())
    return power_sets


if __name__ == "__main__":
    print('--- Part One ---')
    print(part_one('input.txt'))
    print('--- Part Two ---')
    print(part_two('input.txt'))
