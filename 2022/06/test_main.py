import main


def test_part1():
    type = 'packet'
    assert main.find_marker("bvwbjplbgvbhsrlpgdmjqwftvncz", type) == 5
    assert main.find_marker("nppdvjthqldpwncqszvftbrmjlhg", type) == 6
    assert main.find_marker("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", type) == 10
    assert main.find_marker("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", type) == 11


def test_part2():
    type = 'message'
    assert main.find_marker("mjqjpqmgbljsphdztnvjfqwrcgsmlb", type) == 19
    assert main.find_marker("bvwbjplbgvbhsrlpgdmjqwftvncz", type) == 23
    assert main.find_marker("nppdvjthqldpwncqszvftbrmjlhg", type) == 23
    assert main.find_marker("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", type) == 29
    assert main.find_marker("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", type) == 26
