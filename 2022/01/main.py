def part_one(input):
    """Iterate over each line of the input, and group each elf (blank line) in
    order to count calories. Return biggest value.
    """
    with open(input, "r") as file:
        lines = file.readlines()
        most = 0
        count = 0

        for line in lines:
            # Current elf
            if line != "\n":
                count += int(line[:-1])

            # Before new elf/EOF
            if line == "\n" or line == lines[-1]:
                if count > most:
                    most = count
                count = 0

        return most


def part_two(input):
    """Iterate over each line of the input, and group each elf (blank line) in
    order to count calories. Compare count with list with top three results.
    Return sum of the list.
    """
    with open(input, "r") as file:
        lines = file.readlines()
        top_three = [0, 0, 0]
        count = 0

        for line in lines:
            if line != "\n":
                count += int(line[:-1])

            if line == "\n" or line == lines[-1]:
                for i in range(len(top_three)):
                    if count > top_three[i]:
                        top_three[i] = count
                        top_three.sort()
                    count = 0

        return sum(top_three)


if __name__ == "__main__":
    input = "./input.txt"
    print("--- Part One ---")
    print(part_one(input))

    print("--- Part Two ---")
    print(part_two(input))
