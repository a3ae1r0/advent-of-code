import main


def test_part_one():
    assert main.part_one('sample.txt') == 4361

def test_part_two():
    assert main.part_two('sample.txt') == 467835
