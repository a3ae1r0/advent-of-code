import csv


def parse_input(input):
    """Read two columns per line into two lists with both players selection"""
    with open(input, "r") as f:
        data = csv.reader(f, delimiter=" ")
        selected = []
        opponent = []

        for cols in data:
            selected.append(cols[1])
            opponent.append(cols[0])

    return selected, opponent


def calc_outcomes(listA, listB):
    """Get results of each round by comparing both players hands"""
    RESULTS = {"X": "C", "Y": "A", "Z": "B"}
    result = []

    for i in range(len(listA)):
        if ord(listA[i]) == (ord(listB[i]) + 23):
            result.append(3)
        elif listB[i] == RESULTS[listA[i]]:
            result.append(6)
        else:
            result.append(0)

    return result


def calc_shapes(opponent, outcomes):
    """Choose the right shape to play in order to follow the strategy guide and
    obtaint he desired outcome for the round"""
    RESULTS_WIN = {"A": "Y", "B": "Z", "C": "X"}
    RESULTS_LOSE = {"A": "Z", "B": "X", "C": "Y"}
    shapes = []

    for i in range(len(opponent)):
        if outcomes[i] == "X":  # Lose
            shapes.append(RESULTS_LOSE[opponent[i]])
        elif outcomes[i] == "Y":  # Draw
            _ = ord(opponent[i]) + 23
            shapes.append(chr(_))
        else:  # Win
            shapes.append(RESULTS_WIN[opponent[i]])

    return shapes


def points_shapes(shapes):
    """Returns the number of player points based on the hand chosen"""
    total_shapes = 0
    for i in shapes:
        total_shapes += ord(i) - 87

    return total_shapes


def points_results(results):
    """Returns the number of player points based on round results"""
    return sum(results)


def part_one(input):
    """Returns total score of the game based on the strategy guide"""
    selected_shapes, opponent_shapes = parse_input(input)
    results = calc_outcomes(selected_shapes, opponent_shapes)

    return points_shapes(selected_shapes) + points_results(results)


def part_two(input):
    """Returns total score of the game based on the (real) strategy guide"""
    desired_outcomes, opponent_shapes = parse_input(input)
    needed_shapes = calc_shapes(opponent_shapes, desired_outcomes)

    # Convert outcomeos from char to int
    CHAR_POINTS = {"X": 0, "Y": 3, "Z": 6}
    results = []
    for i in desired_outcomes:
        results.append(CHAR_POINTS[i])

    return points_shapes(needed_shapes) + points_results(results)


if __name__ == "__main__":
    input = "./input.txt"
    print("--- Part One ---")
    print(part_one(input))

    print("--- Part Two ---")
    print(part_two(input))
