import string



def read_input(infile: str) -> list:
    """Return string with input from exercise dataset."""
    with open(infile, mode='r') as f:
        return f.read().strip().split('\n')


def part_one(infile: str) -> int:
    data = read_input(infile)
    symbols = string.punctuation.replace('.','')
    total = 0
    # parts = []

    for line in range(len(data)):
        i = 0
        num_limit = []
        while i < len(data[line]):
            if data[line][i].isdigit():
                num_limit = [i]
                while data[line][i].isdigit():
                    if i == len(data[line])-1:
                        i += 1
                        break
                    i += 1
                num_limit.append(i-1)


                # Check left and right
                l = num_limit[0]
                r = num_limit[1]
                if l != 0 and data[line][l-1] in symbols:
                    # parts.append(int(data[line][l:r+1]))
                    total += int(data[line][l:r+1])
                    continue
                if r != len(data[line])-1 and data[line][r+1] in symbols:
                    # parts.append(int(data[line][l:r+1]))
                    total += int(data[line][l:r+1])
                    continue

                # Check above
                if line != 0:
                    for i in range(l-1, r+2):
                        if i >= 0 and i < len(data[line]) and data[line-1][i] in symbols:
                            # parts.append(int(data[line][l:r+1]))
                            total += int(data[line][l:r+1])
                            continue
                # Check below
                if line != len(data)-1:
                    for i in range(l-1, r+2):
                        if i >= 0 and i < len(data[line]) and data[line+1][i] in symbols:
                            # parts.append(int(data[line][l:r+1]))
                            total += int(data[line][l:r+1])
                            continue
            i += 1

    return total


if __name__ == '__main__':
    print('--- Part One ---')
    print(part_one('input.txt'))
    # print('--- Part Two ---')
    # print(part_two('input.txt'))
