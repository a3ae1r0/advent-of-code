import main


def test_part_one():
    assert main.part_one('sample1.txt') == 142

def test_part_two():
    assert main.part_two('sample2.txt') == 281
