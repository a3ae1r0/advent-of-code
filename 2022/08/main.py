def parse_input(input):
    """Read and return input"""
    with open(input, "r") as f:
        return f.read().strip()


def is_visible(i, j, forest, height, length):
    """Check if current tree is visible from all four directions. Imediately
    return - from loop and function - if the tree is visible
    """
    # Control variable (default)
    visible = True

    # Trees above
    for row_up in range(i-1, -1, -1):
        if int(forest[row_up][j]) >= height:
            # Break since we already know that the tree is not visible
            visible = False
            break

    # Tree is visible from above direction. Don't need to continue
    if visible:
        return True
    else:
        # Since the tree it's not visible from the previous direction, we need
        # to do the same verification for the other directions. Reset variable
        visible = True

    # Trees below
    for row_down in range(i+1, length):
        if int(forest[row_down][j]) >= height:
            visible = False
            break

    if visible:
        return True
    else:
        visible = True

    # Trees to the left
    for column_left in range(j-1, -1, -1):
        if int(forest[i][column_left]) >= height:
            visible = False
            break

    if visible:
        return True
    else:
        visible = True

    # Trees to the right
    for column_right in range(j+1, length):
        if int(forest[i][column_right]) >= height:
            visible = False
            break

    # Final check
    return True if visible else False


def calc_scenic_score(i, j, forest, height, length):
    """For each direction calculate the tree scenic score. Return the
    multipling score for that tree
    """
    total_score = 1

    # Trees above
    current_score = 0
    for row_up in range(i-1, -1, -1):
        current_score += 1
        if int(forest[row_up][j]) >= height:
            break

    total_score *= current_score

    # Trees below
    current_score = 0
    for row_down in range(i+1, length):
        current_score += 1
        if int(forest[row_down][j]) >= height:
            break

    total_score *= current_score

    # Trees to the left
    current_score = 0
    for column_left in range(j-1, -1, -1):
        current_score += 1
        if int(forest[i][column_left]) >= height:
            break

    total_score *= current_score

    # Trees to the right
    current_score = 0
    for column_right in range(j+1, length):
        current_score += 1
        if int(forest[i][column_right]) >= height:
            break

    total_score *= current_score

    return total_score


def part_one(input):
    """Iterate over grid and for each tree check it's visible or not by
    comparing heights of the trees in the same row/column.
    Return total count of visible trees
    """
    data = parse_input(input)
    forest = data.splitlines()
    length = len((forest[0]).strip())
    visible_cnt = (length * 2) + ((length-2) * 2)

    for i, row in enumerate(forest):
        for j, column in enumerate(row):
            # Exclude tress around the edge
            if 0 < i < length-1 and 0 < j < length-1:
                height = int(column)

                # Compare heights with surrounding trees in all four directions
                if is_visible(i, j, forest, height, length):
                    visible_cnt += 1

    return visible_cnt


def part_two(input):
    """Iterate over grid and for each tree check the scenic score. Compare with
    top score and return the highest scenic score
    """
    data = parse_input(input)
    forest = data.splitlines()
    length = len((forest[0]).strip())
    max_score = 0

    for i, row in enumerate(forest):
        for j, column in enumerate(row):
            # Exclude tress around the edge
            if 0 < i < length-1 and 0 < j < length-1:
                height = int(column)

                score = calc_scenic_score(i, j, forest, height, length)
                if score > max_score:
                    max_score = score

    return max_score


if __name__ == "__main__":
    input = "./input.txt"
    print("--- Part One ---")
    print(part_one(input))

    print("--- Part Two ---")
    print(part_two(input))
