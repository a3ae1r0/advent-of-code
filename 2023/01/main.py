def read_input(infile: str) -> list:
    """Return string with input from exercise dataset."""
    with open(infile, mode='r') as f:
        return f.read().strip().split('\n')


def part_one(infile: str) -> int:
    """Per line, combine first and last digit into a single two-digit number.
    Sum all digits in the end.

    To this by iterating over string and finding the first and last digit.
    """
    data = read_input(infile)
    calibration_sum = 0
    for line in data:
        calibration_values = ''
        for char in line:
            if char.isdigit():
                calibration_values += char
                break
        for char in line[::-1]:
            if char.isdigit():
                calibration_values += char
                break
        calibration_sum += int(calibration_values)
    return calibration_sum


def part_two(infile: str) -> int:
    """The calibration values in each line could be spelled out with letters
    (e.g. one).

    Iterating over the string and finding the first and last digit, either with
    'digits' or words.
    """
    data = read_input(infile)
    translation = {
        'one': '1',
        'two': '2',
        'three': '3',
        'four': '4',
        'five': '5',
        'six': '6',
        'seven': '7',
        'eight': '8',
        'nine': '9',
    }
    calibration_sum = 0

    for line in data:
        calibration_values = ''

        for char in range(len(line)):
            if line[char].isdigit():
                calibration_values += line[char]
                break
            if (char <= len(line)-3) and line[char:char+3] in translation:
                calibration_values += translation[line[char:char+3]]
                break
            if (char <= len(line)-4) and line[char:char+4] in translation:
                calibration_values += translation[line[char:char+4]]
                break
            if (char <= len(line)-5) and line[char:char+5] in translation:
                calibration_values += translation[line[char:char+5]]
                break

        for char in range(len(line)-1, -1, -1):
            if line[char].isdigit():
                calibration_values += line[char]
                break
            if (char <= len(line)-3) and line[char:char+3] in translation:
                calibration_values += translation[line[char:char+3]]
                break
            if (char <= len(line)-4) and line[char:char+4] in translation:
                calibration_values += translation[line[char:char+4]]
                break
            if (char <= len(line)-5) and line[char:char+5] in translation:
                calibration_values += translation[line[char:char+5]]
                break

        calibration_sum += int(calibration_values)
    return calibration_sum


if __name__ == '__main__':
    print('--- Part One ---')
    print(part_one('input.txt'))
    print('--- Part Two ---')
    print(part_two('input.txt'))
