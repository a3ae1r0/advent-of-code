import main


def test_part_one():
    assert main.part_one('sample.txt') == 8

def test_part_two():
    assert main.part_two('sample.txt') == 2286
