def parse_input(input):
    """Parse input and split relevant sections into variables"""
    with open(input, "r") as f:
        data = f.read()

        # Get crates text only
        _ = data.find(' 1')
        crates = data[:_-1]

        data = data[_:]

        # Get number of lanes
        total_lanes = 0
        for lane in data.split('\n'):
            if lane.startswith(' 1'):
                total_lanes = int(lane[-1:])
                break

        # Get moves text only + parse move instructions
        _ = data.find('move')
        _ = data[_:-1]
        moves = []
        remove_words = ['move', 'from', 'to']

        for move in _.split('\n'):
            move = move.split()
            _ = [word for word in move if word.lower() not in remove_words]
            moves.append(_)

        # Get stack from each lane
        stack_boxes = {}

        for lane in crates.split('\n'):
            if lane[1] != ' ':
                stack_boxes.setdefault(1, "")
                # Insert at the beginning. Left side of string == base of lane
                stack_boxes[1] = lane[1] + stack_boxes[1]

            # Remaining lanes after 1st one
            start = 1
            for remaining in range(total_lanes-1):
                start += 4
                try:
                    if lane[start] != ' ':
                        stack_boxes.setdefault(remaining+2, "")
                        stack_boxes[remaining+2] = (
                            lane[start] + stack_boxes[remaining+2]
                        )
                except Exception:
                    # When the current stack is shorter (string/line ended)
                    pass

    return stack_boxes, moves


def move_boxes(boxes, moves, model):
    """Move boxes based on the input instructions"""
    src = dst = 0
    stack_9001 = ''

    # Move boxes by following instructions moves
    for move in moves:
        number_boxes = move[0]
        for i in range(int(number_boxes)):
            # Remove last char of the str (stop) and append to other stack
            src = move[1]
            dst = move[2]

            _ = boxes[int(src)][-1:]
            boxes[int(src)] = boxes[int(src)][:-1]

            # Move boxes 1-by-1 or append to stack of boxes
            if number_boxes == 1 or model == '9000':
                boxes[int(dst)] += _
            elif model == '9001':
                stack_9001 = _ + stack_9001

        # Move pending boxes in stack
        if stack_9001 != '':
            boxes[int(dst)] += stack_9001
            stack_9001 = ''

    return boxes


def part_one(input):
    """Move boxes by following the instructions"""
    boxes, moves = parse_input(input)
    boxes = move_boxes(boxes, moves, '9000')
    message = ''

    for key in sorted(boxes):
        message += boxes[key][-1:]

    return message


def part_two(input):
    """Move boxes by following the instructions and the new CrateMover 9001!"""
    boxes, moves = parse_input(input)
    boxes = move_boxes(boxes, moves, '9001')
    message = ''

    for key in sorted(boxes):
        message += boxes[key][-1:]

    return message


if __name__ == "__main__":
    input = "./input.txt"
    print("--- Part One ---")
    print(part_one(input))

    print("--- Part Two ---")
    print(part_two(input))
