def read_input(input):
    """Return list with string(lines) read from file"""
    with open(input, "r") as f:
        data = f.readlines()

    return data


def calc_priority(letter):
    """Return the priority of an item based on their type score"""
    if letter.islower():
        return ord(letter) - 96
    else:
        return ord(letter) - 38


def part_one(input):
    """Find the common item between both compartments of the rucksacks"""
    data = read_input(input)
    total = 0

    for line in data:
        middle = int(len(line[:-1]) / 2)
        pocket1 = line[:middle]
        pocket2 = line[middle:]

        for item in pocket1:
            if item in pocket2:
                total += calc_priority(item)
                break  # One item per rucksacks

    return total


def part_two(input):
    """Find the common item in all of the three Elves in rucksacks (group)"""
    data = read_input(input)
    total = 0

    for i in range(0, len(data), 3):
        elf1 = data[i][:-1]
        elf2 = data[i+1][:-1]
        elf3 = data[i+2][:-1]

        for item in elf1:
            if (item in elf2) and (item in elf3):
                total += calc_priority(item)
                break

    return total


if __name__ == "__main__":
    input = "./input.txt"
    print("--- Part One ---")
    print(part_one(input))

    print("--- Part Two ---")
    print(part_two(input))
